using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Compas : MonoBehaviour
{
    Dropdown dropdown;
    void Awake()
    {
        dropdown=GetComponent<Dropdown>();
    }

    public void CheckDropdown()
    {
        if(dropdown.value==0)
        GameController.Instance.compas = GameController.Compas.c4x4;
        if(dropdown.value==1)
        GameController.Instance.compas = GameController.Compas.c8x4;
        GameController.Instance.InitPistas();
    }
}
