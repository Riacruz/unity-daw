using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineTempoDrag : MonoBehaviour
{
    private bool dragging;
    Vector2 target;
    PlayControl playControl;

    private void Start()
    {
        playControl = GameController.Instance.playControl;
    }
   
    public void Update()
    {
        if (dragging)
        {
            target = Camera.main.ScreenToWorldPoint(Input.mousePosition); 
            target.y = transform.position.y;      
            target.x = Mathf.Clamp(target.x,-9.01f, 200);  
            transform.position = target;
        }
    }
    private void OnMouseDown() {
        if(GameController.Instance.playControl.isPlaying)return;
        dragging = true;
    }
    private void OnMouseUp() {
        
        dragging = false;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        
        if(other.TryGetComponent<AudioControl>(out AudioControl audio))
        {
            if(playControl.isPlaying)
            audio.audioSource.Play();
        }
    }
}