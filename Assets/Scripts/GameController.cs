using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }
    public List<SoundTrack> soundTracks;
    public List<AudioControl> audios;
    public float BPM; 
    public Compas compas;
    public Toggle toggle;
    public InputField inputBPM;
    public PlayControl playControl;
    public AudioClip metronomeClip;
    public enum Compas {
        c4x4,
        c8x4
    }
    void Awake()
    {        
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }
        Instance = this;
        soundTracks= new List<SoundTrack>();
        audios= new List<AudioControl>();
        BPM=60;
        compas = Compas.c4x4;
    }

    public void InitPistas()
    {
        foreach(var p in soundTracks)
        {
            p.initPista.DoTempoScale();
        }
    }

    public void SetBPM()
    {
        if(inputBPM.text!="")
        BPM=float.Parse(inputBPM.text);
    }
}
