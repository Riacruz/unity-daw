using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitPista : MonoBehaviour
{
    SoundTrack soundTrack;
    [SerializeField]
    GameObject prefab;
    List<GameObject> listGOsForTempo = new List<GameObject>();
    void Start()
    {
        soundTrack = GetComponentInParent<SoundTrack>();
        DoTempoScale();
    }

    void ClearGOsTempo()
    {
        foreach(var g in listGOsForTempo)
        {
            Destroy(g.gameObject);
        }
    }

    public void DoTempoScale()
    {
        ClearGOsTempo();
        listGOsForTempo = new List<GameObject>();
        int plusI = 2;
        float extra = 0;
        Vector2 scale = Vector2.one;
        if(GameController.Instance.compas==GameController.Compas.c4x4)
        {
            plusI=2;
            scale = Vector2.one;
        }
        else if (GameController.Instance.compas==GameController.Compas.c8x4)
        {
            plusI=1;
            scale = new Vector2(0.5f,1);
            extra=1.25f;
        }

        for (int i=0;i<soundTrack.transform.localScale.x;i=i+plusI)
        {
            GameObject go = Instantiate(prefab,new Vector3(transform.position.x+i+extra, transform.position.y, transform.position.z), Quaternion.identity);
            go.transform.localScale = scale;
            listGOsForTempo.Add(go);
        }
    }
}
