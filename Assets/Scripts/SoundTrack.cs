using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundTrack : MonoBehaviour
{
    public float position;
    public InitPista initPista;
    void Awake()
    {
        initPista = GetComponentInChildren<InitPista>();
        position=transform.position.y;
    }

    void Start()
    {
        GameController.Instance.soundTracks.Add(this);
    }
}
