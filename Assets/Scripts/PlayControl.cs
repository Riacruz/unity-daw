using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleFileBrowser;

public class PlayControl : MonoBehaviour
{
    [SerializeField]
    Text buttonText;
    public bool isPlaying;
    private Vector3 initPosition;
    AudioClip metronomeClip;
    
    void Start()
    {
        initPosition = transform.position;
        metronomeClip = GameController.Instance.metronomeClip;
    }

    private void Update() {
        if(Input.GetButtonDown("Jump"))
        {
            Play();
        }
    }
    public void Play()
    {
        PlayToStop();
        if(isPlaying)
        {
            StopAllCoroutines();   
            foreach(var a in GameController.Instance.audios)a.audioSource.Stop();      
            isPlaying=false;
            return;
        }
        Vector3 targetPos = new Vector3(transform.position.x+60/GameController.Instance.BPM, transform.position.y,transform.position.z);
        StartCoroutine (MoveOverSeconds (gameObject, targetPos, 1f));
        StartCoroutine(ieMetronome(60/GameController.Instance.BPM));
        isPlaying=true;
    }

    public void Back()
    {
        if(isPlaying)Play(); //stop
        transform.position = initPosition;
    }

    void PlayToStop()
    {
        print(buttonText.text);
        if(buttonText.text=="PLAY")
        {
            buttonText.text="STOP";
            return;
        }        
        if(buttonText.text=="STOP")
        buttonText.text="PLAY";
    }

    IEnumerator ieMetronome(float tempo)
    {
        AudioSource.PlayClipAtPoint(metronomeClip,Vector3.zero); 
        yield return new WaitForSecondsRealtime(tempo);
        StartCoroutine(ieMetronome(tempo));
    }
    public IEnumerator MoveOverSeconds (GameObject objectToMove, Vector3 end, float seconds)
    {
               
        float elapsedTime = 0;
        Vector3 startingPos = objectToMove.transform.position;
        while (elapsedTime < seconds)
        {
            objectToMove.transform.position = Vector3.LerpUnclamped(startingPos, end, (elapsedTime / seconds));
            elapsedTime += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        objectToMove.transform.position = end;
        Vector3 targetPos = new Vector3(transform.position.x+60/GameController.Instance.BPM, transform.position.y,transform.position.z);
        StartCoroutine (MoveOverSeconds (gameObject, targetPos, seconds));
        
    }
}
