using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioDrag : MonoBehaviour
{
    private AudioControl audioControl;
    private bool dragging;
    Vector2 target;

    private void Awake()
    {
        audioControl=GetComponent<AudioControl>();
    }
   
    public void Update()
    {
        if (dragging)
        {
            target = Camera.main.ScreenToWorldPoint(Input.mousePosition);         
            transform.position = target;
        }
        
    }
    private void OnMouseOver() {
        if(Input.GetMouseButtonDown(1))
        {
            GetComponent<AudioControl>().audioSource.Play();
            return;
        }else if(Input.GetMouseButtonUp(1))audioControl.audioSource.Stop();
        if(Input.GetKeyDown(KeyCode.D))
        {
            GenerateCopy();
            return;
        }
    }
    private void OnMouseDown()
    {
        
        if(audioControl.isGenerator)
        {
            GenerateCopy();
            return;
        }
        dragging = true;
        
    }

    void GenerateCopy()
    {
        AudioDrag audio = Instantiate<AudioDrag>(this);
        audio.dragging=true;
        audio.audioControl.isGenerator=false;
        audio.GetComponent<AudioControl>().audioSource.Play();
    }
    private void OnMouseUp() {
        dragging = false;
        foreach(var s in GameController.Instance.soundTracks)
        {
            if(Mathf.Abs(transform.position.y-s.position)<1)
            {
                float posX = transform.position.x;
                if(GameController.Instance.toggle.isOn)
                {
                    switch(GameController.Instance.compas)
                    {
                        case GameController.Compas.c4x4:
                        posX=Mathf.Round(transform.position.x)-0.5f;
                        break;
                        case GameController.Compas.c8x4:
                        posX = Mathf.Round(transform.position.x * 2) / 2;
                        break;
                    }
                }
                transform.position = new Vector2(posX, s.transform.position.y);
            }
        }
    }
}