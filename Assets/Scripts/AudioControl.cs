using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControl : MonoBehaviour
{
    public bool isGenerator;
    public AudioSource audioSource;
    void Awake()
    {
        audioSource=GetComponent<AudioSource>();
        print(audioSource.clip.length);
        AdjustScale();
        GameController.Instance.audios.Add(this);
    }

    public void AdjustScale()
    {
        transform.localScale = new Vector3(audioSource.clip.length,1,1);
        if(gameObject.GetComponent<BoxCollider2D>())
        {
            Destroy(GetComponent<BoxCollider2D>());
            gameObject.AddComponent<BoxCollider2D>();
        }
        else gameObject.AddComponent<BoxCollider2D>();
    }

}
